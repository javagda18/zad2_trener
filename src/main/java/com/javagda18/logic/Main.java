package com.javagda18.logic;

import com.javagda18.model.Diet;
import com.javagda18.model.DietType;
import com.javagda18.model.Trainer;
import com.javagda18.model.User;

import java.util.Optional;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        DaoCreateUpdate daoCreateUpdate = new DaoCreateUpdate();
        DaoDeleteGet daoDeleteGet = new DaoDeleteGet();

        Scanner scanner = new Scanner(System.in);

        String command;
        do {
            System.out.println("Podaj komendę:");
            command = scanner.next();

            if (command.equalsIgnoreCase("quit")) {
                // kończymy program po wpisaniu quit.
                break;
            }

            // obsługa pozostałych komend:
            switch (command) {
                case "dodaj_trenera":
                    System.out.println("Podaj imie:");
                    String name = scanner.next();
                    System.out.println("Podaj nazwisko:");
                    String lastName = scanner.next();
                    System.out.println("Podaj ocene:");
                    Double rate = scanner.nextDouble();
                    System.out.println("Czy jest dietetykiem:");
                    boolean dietitian = scanner.nextBoolean();
                    Trainer trainer = new Trainer(name, lastName, rate, dietitian);

                    daoCreateUpdate.saveEntity(trainer);
                    break;
                case "dodaj_uzytkownika":
                    System.out.println("Podaj imie:");
                    name = scanner.next();
                    System.out.println("Podaj nazwisko:");
                    lastName = scanner.next();
                    User user = new User(name, lastName);

                    daoCreateUpdate.saveEntity(user);
                    break;
                case "dodaj_diete":
                    try {
                        System.out.println("Podaj nazwę:");
                        name = scanner.next();
                        System.out.println("Podaj typ diety:");
                        String type = scanner.next();
                        DietType dietType = DietType.valueOf(type.toUpperCase().trim());
                        System.out.println("Podaj kalorycznosc:");
                        double kcal = scanner.nextDouble();
                        System.out.println("Podaj ocene:");
                        rate = scanner.nextDouble();
                        Diet diet = new Diet(name, kcal, dietType, rate);

                        daoCreateUpdate.saveEntity(diet);
                    } catch (IllegalArgumentException iae) {
                        System.out.println("Zła wartość argumentu, pomijam dodawanie.");
                    }
                    break;

                case "przypisz_diete":
                    try {
                        System.out.println("Podaj imie uzytkownika:");
                        name = scanner.next();
                        System.out.println("Podaj nazwisko:");
                        lastName = scanner.next();

                        System.out.println("Podaj typ diety");
                        String type = scanner.next();
                        DietType dietType = DietType.valueOf(type.toUpperCase().trim());
                        System.out.println("Podaj kalorycznosc:");
                        double kcal = scanner.nextDouble();

                        daoDeleteGet.przypisz_diete(name, lastName, dietType, kcal);
//                        Optional<Diet> optionalDiet = daoDeleteGet.getDiet(dietType, kcal);
//                        Optional<User> optionalUser = daoDeleteGet.getUser(name, lastName);
//
//                        if(optionalDiet.isPresent() && optionalUser.isPresent()) {
//                            user = optionalUser.get();
//                            Diet diet = optionalDiet.get();
//
//                            diet.getUserSet().add(user);
//                            user.setDiet(diet);
//
//                            daoCreateUpdate.saveEntity(user);
//                            daoCreateUpdate.saveEntity(diet);
//                        }
                    } catch (IllegalArgumentException iae) {
                        System.out.println("error");
                    }

                default:
                    break;
            }

        } while (!command.equalsIgnoreCase("quit"));

//        daoCreateUpdate.saveEntity(new Diet("Keto 2000", 2000, DietType.KETO, 4.9));
//        daoCreateUpdate.saveEntity(new Diet("Standard 2500", 2500, DietType.STANDARD, 5.2));
//        daoCreateUpdate.saveEntity(new Diet("Sport 3000", 3000, DietType.SPORT, 5.0));
//
//        daoCreateUpdate.saveEntity(new Trainer("Pawel", "Gawel", 1.0, true));
//
//        daoCreateUpdate.saveEntity(new User("Gawel", "Pawel"));

        HibernateUtil.getSessionFactory().close();
    }
}
