package com.javagda18.logic;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

public class HibernateUtil {

    // statyczna fabryka sesji
    private static SessionFactory sessionFactory;

    // blok static (zadeklarowany niżej) wywoła się po pierwszym użyciu klasy
    // HibernateUtil w projekcie
    static {
        // Tworzymy sobie obiekt, który pobiera konfigurację z pliku hibernate cfg xml
        StandardServiceRegistry standardServiceRegistry =
                new StandardServiceRegistryBuilder()
                        .configure(HibernateUtil.class.getResource("/hibernate.cfg.xml")).build();

        // metadata to informacje dotyczące plików. z danych załadowanych wcześniej
        // towrzymy sobie obiekt metadata.
        Metadata metadata = new MetadataSources(standardServiceRegistry)
                .getMetadataBuilder().build();

        // stworzenie sesji z danych zawartych w pliku hibernate cfg xml
        sessionFactory = metadata.getSessionFactoryBuilder().build();
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}

