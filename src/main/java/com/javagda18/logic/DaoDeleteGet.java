package com.javagda18.logic;

import com.javagda18.model.Diet;
import com.javagda18.model.DietType;
import com.javagda18.model.User;
import lombok.extern.java.Log;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;

@Log
public class DaoDeleteGet {

    public <T> List<T> getAll(Class<T> tClass) {
        // pobieramy fabrykę (do tworzenia sesji)
        SessionFactory factory = HibernateUtil.getSessionFactory();
        try (Session session = factory.openSession()) {

            String nazwaKlasy = tClass.getSimpleName();
            Query<T> query = session.createQuery("from " + nazwaKlasy + " o", tClass);  // Student.class

            List<T> objectList = query.list();

            return objectList;
        } catch (HibernateException he) {
            log.log(Level.SEVERE, "Error loading objects.");
        }
        return new ArrayList<>();
    }

    public Optional<Diet> getDiet(DietType dietType, double kcal) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        try (Session session = factory.openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Diet> query = criteriaBuilder.createQuery(Diet.class);
            Root<Diet> root = query.from(Diet.class);

            Predicate p1 = criteriaBuilder.equal(root.get("dietType"), dietType);
            Predicate p2 = criteriaBuilder.equal(root.get("kcal"), kcal);

            // przekazanie predykatów z "and"
            query.select(root).where(criteriaBuilder.and(p1, p2));

            Query<Diet> studentQuery = session.createQuery(query); // .setMaxResults() == limit

            Diet diet = studentQuery.getSingleResult();

            return Optional.of(diet);
        } catch (NoResultException nre) {
            log.log(Level.SEVERE, "No results."); // severe == error
        } catch (HibernateException he) {
            log.log(Level.SEVERE, "Error loading objects.");
        }
        return Optional.empty(); // jeśli się nie uda (z jakiegokolwiek względu) - pusty Optional.
    }

    public Optional<User> getUser(String name, String lastName) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        try (Session session = factory.openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
            Root<User> root = query.from(User.class);

            Predicate p1 = criteriaBuilder.equal(root.get("firstName"), name);
            Predicate p2 = criteriaBuilder.equal(root.get("lastName"), lastName);

            // przekazanie predykatów z "and"
            query.select(root).where(criteriaBuilder.and(p1, p2));

            Query<User> studentQuery = session.createQuery(query); // .setMaxResults() == limit

            User diet = studentQuery.getSingleResult();

            return Optional.of(diet);
        } catch (NoResultException nre) {
            log.log(Level.SEVERE, "No results."); // severe == error
        } catch (HibernateException he) {
            log.log(Level.SEVERE, "Error loading objects.");
        }
        return Optional.empty(); // jeśli się nie uda (z jakiegokolwiek względu) - pusty Optional.
    }

    public boolean przypisz_diete(String firstName, String lastName, DietType dietType, double kcal){
        SessionFactory factory = HibernateUtil.getSessionFactory();
        try (Session session = factory.openSession()) {
            Transaction transaction = session.beginTransaction();

            User user = findUser(session, firstName, lastName);
            Diet diet = findDiet(session, dietType, kcal);

            diet.getUserSet().add(user);
            user.setDiet(diet);

            session.saveOrUpdate(diet);
            session.saveOrUpdate(user);

            transaction.commit();
            return true;
        } catch (NoResultException nre) {
            log.log(Level.SEVERE, "No results."); // severe == error
        } catch (HibernateException he) {
            log.log(Level.SEVERE, "Error loading objects.");
        }
        return false; // jeśli się nie uda (z jakiegokolwiek względu) - pusty Optional.
    }

    private Diet findDiet(Session session, DietType dietType, double kcal) {
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Diet> query = criteriaBuilder.createQuery(Diet.class);
        Root<Diet> root = query.from(Diet.class);

        Predicate p1 = criteriaBuilder.equal(root.get("dietType"), dietType);
        Predicate p2 = criteriaBuilder.equal(root.get("kcal"), kcal);

        // przekazanie predykatów z "and"
        query.select(root).where(criteriaBuilder.and(p1, p2));

        Query<Diet> studentQuery = session.createQuery(query); // .setMaxResults() == limit

        Diet diet = studentQuery.getSingleResult();
        return diet;
    }

    private User findUser(Session session, String firstName, String lastName){
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
        Root<User> root = query.from(User.class);

        Predicate p1 = criteriaBuilder.equal(root.get("firstName"), firstName);
        Predicate p2 = criteriaBuilder.equal(root.get("lastName"), lastName);

        // przekazanie predykatów z "and"
        query.select(root).where(criteriaBuilder.and(p1, p2));

        Query<User> studentQuery = session.createQuery(query); // .setMaxResults() == limit

        User user = studentQuery.getSingleResult();
        return user;
    }
}
