package com.javagda18.logic;

import com.javagda18.model.BaseEntity;
import lombok.extern.java.Log;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.RollbackException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

@Log
public class DaoCreateUpdate {
    public void saveEntity(BaseEntity baseEntity) {
        // pobieramy fabrykę (do tworzenia sesji)
        SessionFactory factory = HibernateUtil.getSessionFactory();

        // tworzymy sesję
        Transaction transaction = null;
        try (Session session = factory.openSession()) {

            // tworzę (rozpoczynam) transakcję
            transaction = session.beginTransaction();

            session.saveOrUpdate(baseEntity);           // zapis obiektu

            transaction.commit();               // commit
        } catch (HibernateException | RollbackException e) {
            log.log(Level.SEVERE, "Error saving object.");
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }
}
