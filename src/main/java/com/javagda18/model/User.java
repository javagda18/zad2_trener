package com.javagda18.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class User extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    private String firstName;
    private String lastName;

    @ManyToOne()
    private Diet diet;

    @ManyToMany(mappedBy = "userSet")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<Trainer> trainers = new HashSet<>();
}
