package com.javagda18.model;


import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Diet extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Diet(String name, double kcal, DietType dietType, double rate) {
        this.name = name;
        this.kcal = kcal;
        this.dietType = dietType;
        this.rate = rate;
    }

    private String name;
    private double kcal;

    @Enumerated(value = EnumType.STRING)
    private DietType dietType;

    private double rate;

    @OneToMany(mappedBy = "diet")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<User> userSet = new HashSet<>();
}
