package com.javagda18.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Trainer extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Trainer(String firstName, String lastName, double rate, boolean dietitian) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.rate = rate;
        this.dietitian = dietitian;
    }

    private String firstName;
    private String lastName;

    private double rate;
    private boolean dietitian;

    @ManyToMany
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<User> userSet = new HashSet<>();
}
